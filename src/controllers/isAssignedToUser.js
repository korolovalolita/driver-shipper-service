export const isAssignedToUser = (req, assignedTo) => {
  if (req.user._id === assignedTo) {
    return true;
  } else {
    return false;
  }
}