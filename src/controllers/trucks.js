import mongoose from "mongoose";
import { TruckSchema } from "../models/truckModel.js";
import { isAssignedToUser } from "./isAssignedToUser.js";
import { checkIfDriver } from "./checkIfDriver.js";

const Truck = mongoose.model("Truck", TruckSchema);

export const createTruck = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      if (!req.body.type) {
        return res.status(400).json({ message: "Specify truck type" });
      }

      const truck = new Truck(req.body);
      truck.created_by = req.user._id;
      truck.type = req.body.type;

      truck.save((err) => {
        if (err) {
          if (err.errors.type) {
            return res.status(400).json({
              message:
                "Truck type should be 'SPRINTER', 'SMALL STRAIGHT' or 'LARGE STRAIGHT'",
            });
          }
          res.status(500).json({ message: "Internal Server Error" });
        } else {
          res.json({ message: "Truck created successfully" });
        }
      });
    }
  });
};

export const getTrucks = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Truck.find({ created_by: req.user._id }, (err, trucks) => {
        if (err) {
          res.status(500).json({ message: "Internal Server Error" });
        } else {
          res.json({
            trucks: trucks.map((truck) => ({
              _id: truck._id,
              created_by: truck.created_by,
              assigned_to: truck.assigned_to,
              type: truck.type,
              status: truck.status,
              created_date: truck.createdDate,
            })),
          });
        }
      });
    }
  });
};

export const getTruckById = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Truck.findById(req.params.id, (err, truck) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!truck) {
          res.status(400).json({ message: "There is no such truck" });
        } else {
          res.json({
            truck: {
              _id: truck._id,
              created_by: truck.created_by,
              assigned_to: truck.assigned_to,
              type: truck.type,
              status: truck.status,
              created_date: truck.createdDate,
            },
          });
        }
      });
    }
  });
};

export const deleteTruckById = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Truck.findByIdAndRemove(req.params.id, (err, truck) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!truck) {
          return res.status(400).json({ message: "There is no such truck" });
        }
        if (isAssignedToUser(req, truck.assigned_to)) {
          return res
            .status(400)
            .json({ message: "You had already been assigned to the load" });
        } else {
          res.json({ message: "Truck deleted successfully" });
        }
      });
    }
  });
};

export const assignTruck = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Truck.findById(req.params.id, (err, truck) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!truck) {
          return res.status(400).json({ message: "There is no such truck" });
        }
        if (isAssignedToUser(req, truck.assigned_to)) {
          return res
            .status(400)
            .json({ message: "Truck had already been assigned to the you" });
        }
        Truck.findOne({ assigned_to: req.user.id }, (err, truck) => {
          if (err) {
            return res.status(500).json({ message: "Internal Server error" });
          }
          if (truck) {
            return res.status(400).json({
              message: "There is a load already been assigned to you",
            });
          }
        });
        truck.assigned_to = req.user._id;
        truck.save();
        res.json({ message: "Truck assigned successfully" });
      });
    }
  });
};

export const editTruck = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Truck.findById(req.params.id, (err, truck) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!truck) {
          return res.status(400).json({ message: "There is no such truck" });
        }
        if (isAssignedToUser(req, truck.assigned_to)) {
          return res
            .status(400)
            .json({ message: "You had already been assigned to the load" });
        } else {
          if (req.body.type) {
            truck.type = req.body.type;
          }
          if (req.body.status) {
            truck.status = req.body.status;
          }
          truck.save();
          res.json({ message: "Truck details changed successfully" });
        }
      });
    }
  });
};
