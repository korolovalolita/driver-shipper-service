import { UserSchema } from "../models/userModel.js";
import mongoose from "mongoose";
const DRIVER = "DRIVER";

const User = mongoose.model("User", UserSchema);

export const checkIfDriver = async (req, res) => {
  const user = await User.findOne({ email: req.user.email }).catch(() => {
    res.status(400).json({ message: "There is no such user" });
  });

  return user.role === DRIVER;
};
