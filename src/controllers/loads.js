import mongoose from "mongoose";
import { LoadSchema } from "../models/loadModel.js";
import { TruckSchema } from "../models/truckModel.js";
import { checkIfDriver } from "./checkIfDriver.js";
import { isNew } from "./isNew.js";

const Load = mongoose.model("Load", LoadSchema);
const Truck = mongoose.model("Truck", TruckSchema);

export const createLoad = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      res.status(400).json({
        message: "You are not a shipper. Only SHIPPERs are allowed to do it",
      });
    } else {
      if (!req.body.name) {
        return res.status(400).json({ message: "Specify load name" });
      }
      if (!req.body.payload) {
        return res.status(400).json({ message: "Specify payload" });
      }
      if (!req.body.pickup_address) {
        return res.status(400).json({ message: "Specify pickup_address" });
      }
      if (!req.body.delivery_address) {
        return res.status(400).json({ message: "Specify delivery_address" });
      }
      if (!req.body.dimensions) {
        return res.status(400).json({ message: "Specify load sizes" });
      }
      if (!req.body.dimensions.width) {
        return res.status(400).json({ message: "Specify load width" });
      }
      if (!req.body.dimensions.length) {
        return res.status(400).json({ message: "Specify load length" });
      }
      if (!req.body.dimensions.height) {
        return res.status(400).json({ message: "Specify load height" });
      }

      const load = new Load(req.body);
      load.created_by = req.user._id;
      load.name = req.body.name;
      load.payload = req.body.payload;
      load.pickup_address = req.body.pickup_address;
      load.delivery_address = req.body.delivery_address;
      load.dimensions.width = req.body.dimensions.width;
      load.dimensions.length = req.body.dimensions.length;
      load.dimensions.height = req.body.dimensions.height;

      load.save((err) => {
        if (err) {
          res.status(500).json({ message: "Internal Server Error" });
        } else {
          res.json({ message: "Load created successfully" });
        }
      });
    }
  });
};

export const getLoads = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      Load.find({ assigned_to: req.user }, (err, loads) => {
        if (err) {
          res.status(500).json({ message: "Internal Server Error" });
        } else {
          res.json({
            loads: loads.map((load) => ({
              _id: load._id,
              created_by: load.created_by,
              assigned_to: load.assigned_to,
              status: load.status,
              state: load.state,
              name: load.name,
              payload: load.payload,
              pickup_address: load.pickup_address,
              dimensions: {
                width: load.dimensions.width,
                length: load.dimensions.length,
                height: load.dimensions.height,
              },
              logs: load.logs,
              created_date: load.created_date,
            })),
          });
        }
      })
        .skip(req.body.offset || 0)
        .limit(req.body.limit || 0);
    } else {
      Load.find({ created_by: req.user }, (err, loads) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server Error" });
        }
        res.json({
          loads: loads.map((load) => ({
            _id: load._id,
            created_by: load.created_by,
            assigned_to: load.assigned_to,
            status: load.status,
            state: load.state,
            name: load.name,
            payload: load.payload,
            pickup_address: load.pickup_address,
            dimensions: {
              width: load.dimensions.width,
              length: load.dimensions.length,
              height: load.dimensions.height,
            },
            logs: load.logs,
            created_date: load.created_date,
          })),
        });
      })
        .skip(req.body.offset || 0)
        .limit(req.body.limit || 0);
    }
  });
};

export const getActiveLoad = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Load.find({ assigned_to: req.user._id }, (err, loads) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server Error" });
        }
        if (!loads) {
          return res.json({ load: {} });
        }
        const load = loads.find((load) => load.status !== "SHIPPED");
        if (!load) {
          return res.json({ load: {} });
        }
        res.json({
          load: {
            _id: load._id,
            created_by: load.created_by,
            assigned_to: load.assigned_to,
            status: load.status,
            state: load.state,
            name: load.name,
            payload: load.payload,
            pickup_address: load.pickup_address,
            dimensions: {
              width: load.dimensions.width,
              length: load.dimensions.length,
              height: load.dimensions.height,
            },
            logs: load.logs,
            created_date: load.createdDate,
          },
        });
      });
    }
  });
};

export const changeLoadState = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (!isDriver) {
      res.status(400).json({
        message: "You are not a driver. Only DRIVERs are allowed to do it",
      });
    } else {
      Load.find({ assigned_to: req.user._id }, (err, loads) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server Error" });
        }
        if (!loads) {
          return res.json({ message: "There are no loads assigned to you" });
        }

        const load = loads.find((load) => load.status !== "SHIPPED");
        if (!load) {
          return res.json({ message: "There are no active loads" });
        }

        if (load.state === "En route to Pick Up") {
          load.status = "ASSIGNED";
          load.state = "Arrived to Pick Up";
          load.logs.push({
            message: "Load state changed to 'Arrived to Pick Up'",
          });
          load.save();
          return res.json({
            message: "Load state changed to 'Arrived to Pick Up'",
          });
        }
        if (load.state === "Arrived to Pick Up") {
          load.state = "En route to delivery";
          load.logs.push({
            message: "Load state changed to 'En route to delivery'",
          });
          load.save();
          return res.json({
            message: "Load state changed to 'En route to delivery'",
          });
        }
        if (load.state === "En route to delivery") {
          load.state = "Arrived to delivery";
          load.status = "SHIPPED";
          Truck.findOne(
            { assigned_to: load.assigned_to, status: "OL" },
            (err, truck) => {
              if (err) {
                return res
                  .status(500)
                  .json({ message: "Internal Server Error" });
              }
              if (!truck) {
                return res.status(500).json({ message: "No such truck found" });
              }
              truck.status = "IS";
              truck.save();
            }
          );
        }
        load.logs.push({
          message: "Load state changed to 'Arrived to delivery'",
        });
        load.save();
        res.json({ message: "Load state changed to 'Arrived to delivery'" });
      });
    }
  });
};

export const getLoadById = (req, res) => {
  Load.findById(req.params.id, (err, load) => {
    if (err) {
      return res.status(500).json({ message: "Internal Server error" });
    }
    if (!load) {
      res.json({ message: "There is no such load" });
    } else {
      if (
        load.created_by === req.user._id ||
        load.assigned_to === req.user._id
      ) {
        res.json({
          load: {
            _id: load._id,
            created_by: load.created_by,
            assigned_to: load.assigned_to,
            status: load.status,
            state: load.state,
            name: load.name,
            payload: load.payload,
            pickup_address: load.pickup_address,
            dimensions: {
              width: load.dimensions.width,
              length: load.dimensions.length,
              height: load.dimensions.height,
            },
            logs: load.logs,
            created_date: load.created_date,
          },
        });
      } else {
        res.status(400).json({ message: "You have no such load" });
      }
    }
  });
};

export const getShippingInfoByLoadId = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      res.status(400).json({
        message: "You are not a shipper. Only SHIPPERs are allowed to do it",
      });
    } else {
      Load.findById(req.params.id, (err, load) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!load) {
          return res.status(400).json({ message: "There is no such load" });
        }
        if (load.status === "NEW") {
          return res.json({ message: "The load is not active yet" });
        } else {
          Truck.findOne({ assigned_to: load.assigned_to }, (err, truck) => {
            if (err) {
              return res.status(500).json({ message: "Internal Server error" });
            }
            if (!truck) {
              return res
                .status(400)
                .json({ message: "There is no truck assigned to the driver" });
            }
            res.json({
              load: {
                _id: load._id,
                created_by: load.created_by,
                assigned_to: load.assigned_to,
                status: load.status,
                state: load.state,
                name: load.name,
                payload: load.payload,
                pickup_address: load.pickup_address,
                delivery_address: load.delivery_address,
                dimensions: {
                  width: load.width,
                  length: load.length,
                  height: load.height,
                },
                logs: load.logs,
                created_date: load.createdDate,
              },
              truck: {
                _id: truck._id,
                created_by: truck.created_by,
                assigned_to: truck.assigned_to,
                type: truck.type,
                status: truck.status,
                created_date: truck.createdDate,
              },
            });
          });
        }
      });
    }
  });
};

export const deleteLoadById = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      res.status(400).json({
        message: "You are not a shipper. Only SHIPPERs are allowed to do it",
      });
    } else {
      Load.findByIdAndRemove(req.params.id, (err, load) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!load) {
          return res.status(400).json({ message: "There is no such load" });
        }
        if (!isNew(load.status)) {
          return res
            .status(400)
            .json({ message: "You cannot delete the load anymore" });
        } else {
          res.json({ message: "Load deleted successfully" });
        }
      });
    }
  });
};

export const editLoad = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      res.status(400).json({
        message: "You are not a shipper. Only SHIPPERs are allowed to do it",
      });
    } else {
      Load.findById(req.params.id, (err, load) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!load) {
          return res.status(400).json({ message: "There is no such load" });
        }
        if (load.created_by !== req.user._id) {
          return res.status(400).json({ message: "You have no such load" });
        }
        if (!isNew(load.status)) {
          return res
            .status(400)
            .json({ message: "You cannot edit the load. It is no longer NEW" });
        } else {
          if (req.body.name) {
            load.name = req.body.name;
          }
          if (req.body.payload) {
            load.payload = req.body.payload;
          }
          if (req.body.pickup_address) {
            load.pickup_address = req.body.pickup_address;
          }
          if (req.body.delivery_address) {
            load.delivery_address = req.body.delivery_address;
          }
          if (req.body.dimensions.width) {
            load.dimensions.width = req.body.dimensions.width;
          }
          if (req.body.dimensions.length) {
            load.dimensions.length = req.body.dimensions.length;
          }
          if (req.body.dimensions.height) {
            load.dimensions.height = req.body.dimensions.height;
          }

          load.save();
          res.json({ message: "Load details changed successfully" });
        }
      });
    }
  });
};

export const postLoad = (req, res) => {
  checkIfDriver(req, res).then((isDriver) => {
    if (isDriver) {
      res.status(400).json({
        message: "You are not a shipper. Only SHIPPERs are allowed to do it",
      });
    } else {
      Load.findById(req.params.id, (err, load) => {
        if (err) {
          return res.status(500).json({ message: "Internal Server error" });
        }
        if (!load) {
          return res.status(400).json({ message: "There is no such load" });
        }
        if (!isNew(load.status)) {
          return res.status(400).json({ message: "The load is no longer NEW" });
        } else if ((load.status = "NEW")) {
          load.status = "POSTED";
          load.driver_found = true;
          Truck.find({ status: "IS" }, (err, trucks) => {
            if (err) {
              return res.status(500).json("Internal Server Error");
            }
            const truck = trucks.find((truck) => truck.assigned_to !== "");
            if (!truck) {
              return res.json({
                message: "There are no trucks assigned to driver",
              });
            }
            truck.status = "OL";
            load.assigned_to = truck.assigned_to;
            truck.save();
            load.save();
            res.json({
              message: "Load posted successfully",
              driver_found: true,
            });
          });
        }
      });
    }
  });
};
