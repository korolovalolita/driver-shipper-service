import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import { UserSchema } from "../models/userModel.js";

const User = mongoose.model("User", UserSchema);
const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i;

export const loginRequired = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    return res.status(400).json({ message: "Unauthorized user" });
  }
};

export const createUser = (req, res) => {
  if (!req.body.email) {
    return res.status(400).json({ message: "Specify email" });
  } 
  if (!emailRegex.test(req.body.email)) {
    return res.status(400).json({ message: "Invalid email" });
  }
  if (!req.body.password) {
    return res.status(400).json({ message: "Specify password" });
  }
  if (!req.body.role) {
    return res.status(400).json({ message: "Specify role" });
  }

  User.findOne({ email: req.body.email }, (err, existingUser) => {
    if (err) {
      console.error(err);
    }
    if (!existingUser) {
      let user = new User(req.body);

      user.hashPassword = bcrypt.hashSync(req.body.password, 10);

      user.save((err, newUser) => {
        if (err) {
          if (err.errors.role) {
            return res.status(400).json({ message: "Specified role should be whether 'DRIVER' or 'SHIPPER'" });
          }
          res.status(400).json({ message: err });
        } else {
          newUser.hashPassword = undefined;
          return res.json({ message: "Success" });
        }
      });
    } else {
      res.status(400).json({ message: "User already exists" });
    }
  });
};

export const loginUser = (req, res) => {
  User.findOne({ email: req.body.email }, (err, user) => {
    if (err) {
      return res.status(500).json({ message: "Internal Server Error" });
    } else {
      if (user) {
        if (user.comparePassword(req.body.password, user.hashPassword)) {
          res.status(200).json({ jwt_token: jwt.sign(
              { _id: user._id, email: user.email },
              "HW2"
            ),
          });
        } else {
          res.status(400).json({ message: "Wrong password" });
        }
      } else {
        res.status(400).json({ message: "User not found" });
      }
    }
  });
};
