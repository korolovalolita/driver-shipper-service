import mongoose from "mongoose";
import { UserSchema } from "../models/userModel.js";
import bcrypt from "bcrypt";

const User = mongoose.model("User", UserSchema);

export const getUser = (req, res) => {
  User.findOne({ email: req.user.email }, (err, user) => {
    if (err) {
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      if (!user) {
        res.status(400).json({ message: "User not found" });
      } else {
        res.json({
          user: {
            _id: user._id,
            role: user.role,
            email: user.email,
            createdDate: user.createdDate,
          },
        });
      }
    }
  });
};

export const deleteUser = (req, res) => {
  User.findOneAndDelete({ email: req.user.email }, (err, user) => {
    if (err) {
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      if (!user) {
        res.status(400).json({ message: "User not found" });
      } else {

        res.json({ message: "Success" });
      }
    }
  });
};

export const changePassword = (req, res) => {
  console.log(1111);
  User.findOne({ email: req.user.email }, (err, user) => {
    if (err) {
      res.status(500).json({ message: "Internal Server Error" });
    } else {
      if (!user) {
        res.status(400).json({ message: "User not found" });
      } else {
        if (req.body.oldPassword) {
          if (req.body.newPassword) {
            if (user.comparePassword(req.body.oldPassword, user.hashPassword)) {
              user.hashPassword = bcrypt.hashSync(req.body.newPassword, 10);
              user.save();
              res.json({ message: "Password changed successfully" });
            } else {
              res.status(400).json({ message: "Old password is wrong" });
            }
          } else {
            res.status(400).json({ message: "Please enter new password" });
          }
        } else {
          res.status(400).json({ message: "Please enter old password" });
        }
      }
    }
  });
};
