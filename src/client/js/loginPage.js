import { mePage } from "./mePage.js";
import { registerPage } from "./registerPage.js";

export const loginPage = () => {
  const root = document.getElementById("root");
  root.innerHTML = "";

  const pageContent = `
    <form>
      <input type="email" placeholder="email" id="email">
      <input type="password" id="password" placeholder="password">
      <input type="submit" value="log in">
      <input type="button" id="register" value="I don't have an account yet">
    </form>`;

  const formwrapper = document.createElement("div");
  formwrapper.id = "login";
  formwrapper.innerHTML = pageContent;
  formwrapper.querySelector("form").onsubmit = (event) => {
    event.preventDefault();

    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    fetch("http://localhost:8080/api/auth/login", {
      method: "post",
      body: JSON.stringify({
        email: email,
        password: password,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (response.message === "User not found") {
          alert("User not found");
        }
        if (response.message === "Wrong password") {
          alert("Wrong password");
        }
        if (response.jwt_token) {
          localStorage.setItem("jwt_token", response.jwt_token);
          mePage();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  formwrapper.querySelector("#register").onclick = (event) => {
    event.preventDefault();
    registerPage();
  };
  root.append(formwrapper);
};
