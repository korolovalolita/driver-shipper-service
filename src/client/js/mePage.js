import { loginPage } from "./loginPage.js";
import { changePasswordPage } from "./changePasswordPage.js";

export const mePage = () => {
  fetch("http://localhost:8080/api/users/me", {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `JWT ${localStorage.getItem("jwt_token")}`
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      if (response.message === "Unauthorized user") {
        loginPage();
      } else {
        const root = document.getElementById("root");
        root.innerHTML = "";
        const pageContent = `
    <header>
      <h2>Hi ${response.user.email.replace(/(@)(.+){1}$/i, "")}!</h2>
      <input type="button" id="changePassword" value="change password">
    </header>
    <nav>
        <ul>
          <li><a href="#" id="trucks">TRUCKS</a></li>
          <li><a href="#" id="loads">LOADS</a></li>
        </ul>
    </nav>`;
        const page = document.createElement("div");
        page.id = "userPage";
        page.innerHTML = pageContent;
        const changePasswordBtn = (page.querySelector(
          "#changePassword"
        ).onclick = (event) => {
          event.preventDefault();
          changePasswordPage();
        });
        root.append(page);
      }
    })
    .catch((err) => {
      console.log(err);
    });
  // };
  // formwrapper.querySelector("#register").onclick = (event) => {
  //   event.preventDefault();
  //   registerPage();
  // };
  // root.append(formwrapper);
};
