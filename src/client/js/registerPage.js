import { loginPage } from "./loginPage.js";

export const registerPage = () => {
  const form = document.querySelector("#login");
  if (form) {
    form.innerHTML = "";
  }
  const pageContent = `
  <form>
    <input type="email" required placeholder="email" id="email">
    <input type="password" required id="password" placeholder="password">
    <select type="password" required id="role" placeholder="your role">
      <option value="" disabled selected>Select your role</option>
      <option value="DRIVER">DRIVER</option>
      <option value="SHIPPER">SHIPPER</option>
    </select>
    <input type="submit" value="sign in">
  </form>`;

  const formwrapper = document.createElement("div");
  formwrapper.innerHTML = pageContent;
  formwrapper.querySelector("form").onsubmit = (event) => {
    event.preventDefault();

    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const role = document.getElementById("role").value;

    fetch("http://localhost:8080/api/auth/register", {
      method: "post",
      body: JSON.stringify({
        email: email,
        password: password,
        role: role,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        alert(response.message);
        if(response.message === "Success") {
          loginPage();
        }
      })
      .catch((err) => {
        console.log(err);
      })
  };
  root.append(formwrapper);
};
