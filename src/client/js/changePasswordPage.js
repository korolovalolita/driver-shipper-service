export const changePasswordPage = () => {
  const root = document.getElementById("root");
  root.innerHTML = "";

  const pageContent = `
    <form>
      <input type="password" placeholder="oldPassword" id="oldPassword">
      <input type="password" id="newPassword" placeholder="newPassword">
      <input type="submit" value="save">
    </form>`;

  const formwrapper = document.createElement("div");
  formwrapper.id = "changePassword";
  formwrapper.innerHTML = pageContent;
  formwrapper.querySelector("form").onsubmit = (event) => {
    event.preventDefault();

    const oldPassword = document.getElementById("oldPassword").value;
    const newPassword = document.getElementById("newPassword").value;

    fetch("http://localhost:8080/api/users/me/password", {
      method: "PATCH",
      body: JSON.stringify({
        oldPassword: oldPassword,
        newPassword: newPassword,
      }),
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": `JWT ${localStorage.getItem("jwt_token")}`,
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        alert(response.message);
        if(response.message === "Password changed successfully") {
          mePage();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  root.append(formwrapper);
};
