import { loginPage } from "./loginPage.js";
import { mePage } from "./mePage.js";

export const initial = () => {
  fetch("http://localhost:8080/api/users/me", {
    method: "get",
    headers: {
      "Content-Type": "application/json",
      "Authorization": `JWT ${localStorage.getItem("jwt_token")}`
    },
  })
    .then((response) => {
      return response.json();
    })
    .then((response) => {
      if (response.message === "Unauthorized user") {
        loginPage();
      } else {
        mePage();
      }

    });
};
initial();
