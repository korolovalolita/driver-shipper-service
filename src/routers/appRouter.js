import { createUser, loginUser, loginRequired } from "../controllers/auth.js";
import {
  changeLoadState,
  createLoad,
  deleteLoadById,
  editLoad,
  getActiveLoad,
  getLoadById,
  getLoads,
  getShippingInfoByLoadId,
  postLoad,
} from "../controllers/loads.js";
import { changePassword, deleteUser, getUser } from "../controllers/me.js";
import {
  createTruck,
  getTrucks,
  getTruckById,
  deleteTruckById,
  editTruck,
  assignTruck,
} from "../controllers/trucks.js";

const routes = (app) => {
  app.route("/api/auth/register").post(createUser);

  app.route("/api/auth/login").post(loginUser);

  app
    .route("/api/users/me")
    .get(loginRequired, getUser)
    .delete(loginRequired, deleteUser);

  app.route("/api/users/me/password")
    .patch(loginRequired, changePassword);

  app
    .route("/api/trucks")
    .get(loginRequired, getTrucks)
    .post(loginRequired, createTruck);

  app
    .route("/api/trucks/:id")
    .get(loginRequired, getTruckById)
    .put(loginRequired, editTruck)
    .delete(loginRequired, deleteTruckById);

  app.route("/api/trucks/:id/assign").post(loginRequired, assignTruck);

  app
    .route("/api/loads")
    .get(loginRequired, getLoads)
    .post(loginRequired, createLoad);

  app.route("/api/loads/active").get(loginRequired, getActiveLoad);

  app.route("/api/loads/active/state").patch(loginRequired, changeLoadState);

  app
    .route("/api/loads/:id")
    .get(loginRequired, getLoadById)
    .put(loginRequired, editLoad)
    .delete(loginRequired, deleteLoadById);

  app.route("/api/loads/:id/post").post(loginRequired, postLoad);

  app.route("/api/loads/:id/shipping_info").get(loginRequired, getShippingInfoByLoadId);
};

export default routes;
