import mongoose from "mongoose";

const Schema = mongoose.Schema;

export const TruckSchema = new Schema({
  type: {
    type: String,
    required: true,
    enum: ["SPRINTER", "SMALL STRAIGHT", "LARGE STRAIGHT"],
  },
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: ""
  },
  status: {
    type: String,
    enum: ["IS", "OL"],
    default: "IS",
  },
  createdDate: {
    type: Date,
    default: Date.now,
  }
});
