import mongoose from "mongoose";

const Schema = mongoose.Schema;

export const LoadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: ""
  },
  status: {
    type: String,
    enum: ["NEW", "POSTED", "ASSIGNED", "SHIPPED"],
    default: "NEW"
  },
  state: {
    type: String,
    enum: ["En route to Pick Up", "Arrived to Pick Up", "En route to delivery", "Arrived to delivery"],
    default: "En route to Pick Up"
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {
      type: Number,
      required: true
    },
    length: {
      type: Number,
      required: true
    },
    height: {
      type: Number,
      required: true
    }
  },
  logs: [{
    message: {
      type: String,
    },
    time: {
      type: Date,
      required: true,
      default: Date.now
    }
  }],
  created_date: {
    type: Date,
    default: Date.now,
  }
})